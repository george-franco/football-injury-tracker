from fastapi import APIRouter
import requests, json


router = APIRouter(tags=["Athletes"])


@router.get("/athletes")
async def get_athletes():
    url = "https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/athletes"
    response = requests.get(url)
    content = json.loads(response.content)
    return content


@router.get("/athletes/{athlete_id}")
async def get_athlete(athlete_id: int):
    url = f"https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/athletes/{athlete_id}"
    response = requests.get(url)
    content = json.loads(response.content)
    return content
