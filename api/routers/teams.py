from fastapi import APIRouter
import requests, json


router = APIRouter(tags=["Teams"])


@router.get("/teams/{team_id}/injuries")
async def get_team_injuries(team_id: int):
    url = f"https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/teams/{team_id}/injuries"
    response = requests.get(url)
    content = json.loads(response.content)
    return content


@router.get("/teams")
async def get_teams():
    url = "https://site.api.espn.com/apis/site/v2/sports/football/nfl/teams"
    response = requests.get(url)
    content = json.loads(response.content)
    return content


@router.get("/teams/{team_id}")
async def get_team(team_id: int):
    url = f"https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/teams/{team_id}"
    response = requests.get(url)
    content = json.loads(response.content)
    return content