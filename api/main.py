from fastapi import FastAPI
from routers import athletes, teams
from fastapi.middleware.cors import CORSMiddleware
import os


app = FastAPI()


app.include_router(athletes.router)
app.include_router(teams.router)


origins = [
    "http://localhost:8000",
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
