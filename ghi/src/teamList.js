import React, { useEffect, useState } from "react";
import InjuryList from "./injury_list";


function TeamList() {
    let [data, setData] = useState([])
    let [teams, setTeams] = useState([])
    const [isLoading, setIsLoading] = useState(false);
    

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            for (let i = 1; i < 31; i++) {
                const res = await fetch(
                    `http://sports.core.api.espn.com/v2/sports/football/leagues/nfl/seasons/2022/teams/${i}?lang=en&region=us`
                    );
                const json = await res.json();
                setTeams((teams) => [...teams, json.displayName]);
                setData((data) => [...data, json]);
        }};
        fetchData();
        setIsLoading(false);
        }, []);

    


    return (
        <>
        {isLoading ? (
            <p>Loading ...</p>
          ) :
        <div className="container">
            <h1>Team List</h1>
            <div>
            <ul>
                {teams.map(team => (
                    <li key={team} style={{ listStyleType: "none" }}>{team}</li>
                ))}
            </ul>
            </div>
            <div>
                <InjuryList props={data} teams={teams}/>
            </div>
        </div>
        }</>
    )
}


export default TeamList;