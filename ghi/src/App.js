import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import InjuryList from './injury_list';
import TeamList from './teamList';

function App() {
  return (
    <div className="App">
      <header className="header">
        <h1>
          Injury Tracker application
        </h1>
      </header>
      <div>

      </div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={< TeamList/>} />
            <Route element={<InjuryList />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;