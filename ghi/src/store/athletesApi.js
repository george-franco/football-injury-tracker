import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'


export const athletesApi = createApi({
  reducerPath: '/athletes',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/athletes'    
  }),
  endpoints: builder => ({
    getAthletes: builder.query({
      query: () => '/athletes',
    }),
  })
});


export const {
    useGetAthletesQuery
} = athletesApi