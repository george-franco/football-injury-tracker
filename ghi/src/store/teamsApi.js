import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'


export const teamsApi = createApi({
  reducerPath: '/teams',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/'
  }),
  endpoints: builder => ({
    getTeams: builder.query({
      query: () => '/teams',
    }),
    getTeam: builder.query({
      query: (team_id) => `/teams/${team_id}`,
    }),
  })
});


export const {
    useGetTeamsQuery,
    useGetTeamQuery,
} = teamsApi