import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query'
import { athletesApi } from './athletesApi';
import { teamsApi } from './teamsApi';


export const store = configureStore({
    reducer: {
        [athletesApi.reducerPath]: athletesApi.reducer,
        [teamsApi.reducerPath]: teamsApi.reducer,
    },
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware()
            .concat(athletesApi.middleware)
            .concat(teamsApi.middleware)
});


setupListeners(store.dispatch);